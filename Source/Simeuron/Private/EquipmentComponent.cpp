// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipmentComponent.h"


#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/Character.h"

UEquipmentComponent::UEquipmentComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UEquipmentComponent::AddItemToInventory(UItemBase_DataAsset* Item)
{
	UWeapon_DataAsset* Weapon = Cast<UWeapon_DataAsset>(Item);
	if(Weapon)
	{
		FWeaponData Data = FWeaponData(Weapon);
		AddWeaponToInventory(&Data);
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Ammo test count: %d"), Data.CurrentAmmo));
	}
}

void UEquipmentComponent::SwitchWeaponNext()
{
	if(Weapons.Num() > 1)
		SwitchWeapon(CurrentWeaponID + 1);
}

void UEquipmentComponent::SwitchWeaponPrevious()
{
	if(Weapons.Num() > 1)
		SwitchWeapon(CurrentWeaponID - 1);
}

int UEquipmentComponent::GetAmmoForCurrentWeapon()
{
	if(!ensure(GetCurrentWeapon()))
		return -1;
	
	EWeaponType TmpWeaponType = GetCurrentWeapon()->GetData()->WeaponDataAsset->WeaponType;
	int* OutAmmo = Ammo.Find(TmpWeaponType);
	if(OutAmmo)
	{
		return *OutAmmo;
	}
	return -1;
}

void UEquipmentComponent::DecreaseAmmoForCurrentWeapon(int InAmmoCount)
{
	Ammo.Add(GetCurrentWeapon()->GetData()->WeaponDataAsset->WeaponType, GetAmmoForCurrentWeapon() - InAmmoCount);
}

void UEquipmentComponent::BeginPlay()
{
	Super::BeginPlay();
	AddItemToInventory(DefaultWeapon);
}

void UEquipmentComponent::AddWeaponToInventory(FWeaponData* InWeaponData)
{
	// Remove weapon from inventory if has the same type as new adding weapon - only one weapon per type
	for(int i = 0; i < Weapons.Num(); i++)
	{
		if(InWeaponData->WeaponDataAsset->WeaponType == Weapons[i].WeaponDataAsset->WeaponType)
		{
			Weapons.RemoveAt(i);
		}
	}
	if(!CurrentWeaponRef)
	{
		// Spawn new weapon
		CurrentWeaponRef = GetWorld()->SpawnActor<AWeaponBase>(AWeaponBase::StaticClass());
		ACharacter* CharacterRef = Cast<ACharacter>(GetOwner());
		if(ensure(CharacterRef))
		{
			CurrentWeaponRef->InitAfterSpawn(CharacterRef);
		}
		// TODO: There shouldn't be const text as socket names - maybe move that names to DA with common names (not exist yet)
		// Hide arm and attach spawned weapon to skeleton
		const USkeletalMeshSocket* WeaponSocket = Cast<ACharacter>(GetOwner())->GetMesh()->GetSocketByName(FName("RightWeaponSocket"));
		if(ensure(WeaponSocket))
		{
			WeaponSocket->AttachActor(CurrentWeaponRef,Cast<ACharacter>(GetOwner())->GetMesh());
			Cast<ACharacter>(GetOwner())->GetMesh()->HideBoneByName(FName("lowerarm_r"), PBO_None);
		}
	}
	// Add new weapon to all weapons array
	const int32 index = Weapons.Add(*InWeaponData);

	// Calculate addition of ammo from pickup and from inventory with the same type
	// Pickup has always full Magazine (defined in FWeaponData constructor)
	// and ammo in magazine is not counting as ammo in inventory
	const EWeaponType WeaponTypeTemp = InWeaponData->WeaponDataAsset->WeaponType;
	int NewAmmoCount = InWeaponData->WeaponDataAsset->AmmoInPickup;
	int* CurrentCount = Ammo.Find(WeaponTypeTemp);
	if(CurrentCount)
	{
		NewAmmoCount += *CurrentCount;
	}
	Ammo.Add(WeaponTypeTemp, NewAmmoCount);

	CurrentWeaponID = index;
	SwitchWeapon(CurrentWeaponID);
}

void UEquipmentComponent::SwitchWeapon(int Index)
{
	// TODO: Powinno być przerywane strzelanie i odtwarzana animacja zmiany broni
	if(!CurrentWeaponRef || Weapons.Num() == 0)
		return;
	
	if(Weapons.Num() > 1)
	{
		if(Index < 0)
		{
			Index += Weapons.Num(); 
		}
		CurrentWeaponID = Index % Weapons.Num();
	}
	FWeaponData *InWeaponData = &Weapons[CurrentWeaponID];
	CurrentWeaponRef -> SetWeaponData(InWeaponData);
	const FString WeaponName = CurrentWeaponRef->ItemData->Name.ToString();
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Changed weapon to: %s"), *WeaponName));
}

