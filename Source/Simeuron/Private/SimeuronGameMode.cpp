// Copyright Epic Games, Inc. All Rights Reserved.

#include "SimeuronGameMode.h"
#include "SimeuronCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASimeuronGameMode::ASimeuronGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
