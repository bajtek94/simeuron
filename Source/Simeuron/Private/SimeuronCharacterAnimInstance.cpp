// Fill out your copyright notice in the Description page of Project Settings.


#include "SimeuronCharacterAnimInstance.h"

#include "GameFramework/PawnMovementComponent.h"

USimeuronCharacterAnimInstance::USimeuronCharacterAnimInstance()
{
	bShootingPose = false;
	CharacterYaw = 0.f;
	CharacterYawOnStop = 0.f;
	
}

void USimeuronCharacterAnimInstance::NativeInitializeAnimation()
{
	TrySetupReferences();
}

void USimeuronCharacterAnimInstance::UpdateAnimationProperties(float DeltaTime)
{
	if(CharacterRef == nullptr || CurrentWeaponData == nullptr)
	{
		TrySetupReferences();
	}
	if(CharacterRef && CurrentWeaponData)
	{
		bIsInAir = CharacterRef->GetMovementComponent()->IsFalling();
		FVector Velocity = CharacterRef->GetVelocity();
		Velocity.Z = 0.f;
		Speed = Velocity.Size();
		Direction = CalculateDirection(Velocity, CharacterRef->GetActorRotation());
		bShootingPose = CharacterRef->bShootingMode;
		if(CurrentWeaponData)
		{
			BlendTimeToShootingPose = CurrentWeaponData->BlendTimeToShootingPose;
		}
	}
	if(CharacterRef)
	{
		CharacterYaw = CharacterRef->GetActorRotation().Yaw;
		if (Speed <= MoveDetectTolerance)
		{
			bIsMoving = false;
		}
		else
		{
			bIsMoving = true;
		}
		const float DeltaRotation = FMath::Abs(CharacterYaw-CharacterYawLastFrame);
		if (DeltaRotation > 0.1f && !bIsTurning)
		{
			bIsTurning = true;
		}
		else if (DeltaRotation < 0.05f)
		{
			if(bIsTurning)
			{
				bIsTurning = false;
			}
			CharacterYawOnStop = FMath::FInterpConstantTo(CharacterYawOnStop, CharacterYaw, DeltaTime, 30.f);
		}
		
		GEngine->AddOnScreenDebugMessage(2, 5.0f, FColor::Yellow, FString::Printf(TEXT("delta rotation: %f, YawOnStop: %f"), DeltaRotation, CharacterYawOnStop));

		TurnInPlace();
		CharacterYawLastFrame = CharacterYaw;
	}
}

void USimeuronCharacterAnimInstance::TurnInPlace()
{
	if (Speed == 0)
	{
		float RootYawOffset =FRotator::NormalizeAxis(CharacterYaw - CharacterYawOnStop);
		const FVector2D InRange {-180.f, 180.f};
		// TODO: Na razie dlugosc animacji obrotu jest ustawiona na sztywno: 7.5f. Da się inaczej?
		const FVector2D OutRange {0, 7.5f};
		YawOffsetConvertedToAnimLength = FMath::GetMappedRangeValueClamped(InRange, OutRange, RootYawOffset);

		GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Rotation offset: %f; CharacterYaw: %f, CurrentAnimTime: %f"),RootYawOffset, CharacterYaw, YawOffsetConvertedToAnimLength));
	}
	else
	{
		CharacterYawOnStop = CharacterYaw;
	}
}

void USimeuronCharacterAnimInstance::TrySetupReferences()
{
	if(!CharacterRef)
	{
		CharacterRef = Cast<ASimeuronCharacter>(TryGetPawnOwner());
	}
	if(CharacterRef)
	{
		UEquipmentComponent* EquipmentComp =  CharacterRef->GetEquipmentComponent();
		if(EquipmentComp)
		{
			AWeaponBase* CurrentWeapon = EquipmentComp->GetCurrentWeapon();
			if(CurrentWeapon)
			{
				CurrentWeaponData = Cast<UWeapon_DataAsset>(CurrentWeapon->ItemData);
			}
		}
	}
}
