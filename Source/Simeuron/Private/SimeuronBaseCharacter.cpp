// Fill out your copyright notice in the Description page of Project Settings.


#include "SimeuronBaseCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ASimeuronBaseCharacter::ASimeuronBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));

}

// Called when the game starts or when spawned
void ASimeuronBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASimeuronBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(bShouldInterpolating)
	{
		float CurrentSpeed = Cast<UCharacterMovementComponent>(GetCharacterMovement())->MaxWalkSpeed;
		Cast<UCharacterMovementComponent>(GetCharacterMovement())->MaxWalkSpeed = FMath::FInterpTo(CurrentSpeed, TargetSpeed, DeltaTime, 5.0f);
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Speed: %f"), CurrentSpeed));
		if(FMath::IsNearlyEqual(TargetSpeed, CurrentSpeed, 1.f))
		{
			Cast<UCharacterMovementComponent>(GetCharacterMovement())->MaxWalkSpeed = TargetSpeed;
			bShouldInterpolating = false;
		}
	}

}

// Called to bind functionality to input
void ASimeuronBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ASimeuronBaseCharacter::SpeedInterpolation(float NewSpeed)
{
	TargetSpeed = NewSpeed;
	float CurrentSpeed = Cast<UCharacterMovementComponent>(GetCharacterMovement())->MaxWalkSpeed;
	if(FMath::IsNearlyEqual(TargetSpeed, CurrentSpeed, 0.1f))
	{
		Cast<UCharacterMovementComponent>(GetCharacterMovement())->MaxWalkSpeed = TargetSpeed;
	}
	else
	{
		bShouldInterpolating = true;
	}
	
}
