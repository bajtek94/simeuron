// Fill out your copyright notice in the Description page of Project Settings.


#include "ReloadWeapon_NotifyState.h"

#include "SimeuronCharacter.h"
#include "Kismet/GameplayStatics.h"

void UReloadWeapon_NotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
                                            float TotalDuration)
{
	// TODO: Zmienic na funkcje z interfejsu zamiast castowac konkretnie na 1 klase
	// Play sound at begin reload
	ASimeuronCharacter* TmpCharacter = Cast<ASimeuronCharacter>(MeshComp->GetOwner());
	if(TmpCharacter)
	{
		EWeaponType TmpWeaponType = TmpCharacter->GetEquipmentComponent()->GetCurrentWeapon()->GetData()->WeaponDataAsset->WeaponType;
		if(TmpCharacter->GetEquipmentComponent()->EquipmentDefaults->ReloadSounds.Contains(TmpWeaponType))
		{
			USoundCue* TmpReloadSound = *TmpCharacter->GetEquipmentComponent()->EquipmentDefaults->ReloadSounds.Find(TmpWeaponType);
			UGameplayStatics::PlaySoundAtLocation(TmpCharacter->GetWorld(), TmpReloadSound,  TmpCharacter->GetActorLocation());
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Not set RELOAD SOUND for: %s in EquipmentDefaults Data Asset. Check EquipmentComponent in Character BP"), *UEnum::GetValueAsString(TmpWeaponType));
		}
	}
}

void UReloadWeapon_NotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	// TODO: Zmienic na funkcje z interfejsu zamiast castowac konkretnie na 1 klase
	// Update ammo values
	ASimeuronCharacter* TmpCharacter = Cast<ASimeuronCharacter>(MeshComp->GetOwner());
	if(TmpCharacter)
	{
		TmpCharacter->GetEquipmentComponent()->GetCurrentWeapon()->FinishReloading();
	}
}
