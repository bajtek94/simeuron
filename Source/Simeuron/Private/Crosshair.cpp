// Fill out your copyright notice in the Description page of Project Settings.


#include "Crosshair.h"

#include "SimeuronCharacter.h"
#include "Camera/CameraComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ACrosshair::ACrosshair()
{
	PrimaryActorTick.bCanEverTick = true;
	CrosshairWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Crosshair"));
	RootComponent = CrosshairWidget;
}

// Called when the game starts or when spawned
void ACrosshair::BeginPlay()
{
	Super::BeginPlay();
	BeginDrawSize = CrosshairWidget->GetDrawSize();
}

// Called every frame
void ACrosshair::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(CharacterRef)
	{
		// Set rotation of crosshair aligned to camera view;
		SetActorRotation(FRotator(0.f, CharacterRef->GetFollowCamera()->GetComponentRotation().Yaw, 0.f));

		// Check location of crosshair and change it if it is out of range
		float DistCrosshairFromPlayer = UKismetMathLibrary::Sqrt(FMath::Square(LastMovedOffset.X)+FMath::Square(LastMovedOffset.Y));
		if(DistCrosshairFromPlayer > MaxDistance)
		{
			LastMovedOffset*= MaxDistance / DistCrosshairFromPlayer;
		}
		const FVector CorrectedCrosshairLocation = CharacterRef->GetActorLocation() + GetActorForwardVector()*LastMovedOffset.Y + GetActorRightVector()*LastMovedOffset.X;
		SetActorLocation(CorrectedCrosshairLocation);

		/// Scale crosshair depends on character speed
		const FVector2D SpeedRange { CharacterRef->WalkSpeed, CharacterRef->GetRunSpeed()};
		const FVector2D SpeedRangeConvertedToScale { 0.f, 1.f};
		float Alpha = FMath::GetMappedRangeValueClamped(SpeedRange, SpeedRangeConvertedToScale, CharacterRef->GetVelocity().Size2D());
		SetScale(Alpha);	
	}
	/// Interp size of crosshair if it should be changed e.g. when shoot
	if(bScalingByImpulse)
	{
		CurrentAdditionalScaleFromImpulse = FMath::FInterpTo(CurrentAdditionalScaleFromImpulse, AdditionalScaleFromImpulse, DeltaTime, ScaleInterpSpeed);
	}
	else
	{
		CurrentAdditionalScaleFromImpulse = FMath::FInterpTo(CurrentAdditionalScaleFromImpulse, 0.f, DeltaTime, ScaleInterpSpeed);
	}
}

void ACrosshair::Impulse()
{
	bScalingByImpulse = true;
	GetWorldTimerManager().SetTimer(Timer_RevertScale, this, &ACrosshair::StopImpulse, TimeToRevertAfterImpulse);
}

void ACrosshair::StopImpulse()
{
	bScalingByImpulse = false;
}

void ACrosshair::Move(float X, float Y, bool bGamepad)
{
	FVector2D CrosshairMoveCorrected = FVector2D(X, Y);
	if(CharacterRef->IsWalkingModeOn())
	{
		CrosshairMoveCorrected *= SpeedMultiplierOnWalk;
	}
	if(bGamepad)
	{
		CrosshairMoveCorrected *= CursorSpeed_Gamepad * GetWorld()->GetDeltaSeconds();
	}
	else
	{
		CrosshairMoveCorrected *= CursorSpeed_Mouse;
	}
	LastMovedOffset = FVector2D(LastMovedOffset.X + CrosshairMoveCorrected.X , LastMovedOffset.Y + CrosshairMoveCorrected.Y);
}

void ACrosshair::SetScale(float Alpha)
{
	Alpha = FMath::Clamp(Alpha, 0.f, 1.f);
	const FVector2D AlphaRange { 0.f, 1.f};
	const FVector2D ScaleRange { 1.f, 1.3f};
	float ScaleMultiplier = FMath::GetMappedRangeValueClamped(AlphaRange, ScaleRange, Alpha);
	ScaleMultiplier += CurrentAdditionalScaleFromImpulse;
	FVector2D NewSize = BeginDrawSize * ScaleMultiplier;
	CrosshairWidget->SetDrawSize(NewSize);
}
