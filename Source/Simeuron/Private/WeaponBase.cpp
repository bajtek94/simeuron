// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"


#include "SimeuronCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
	//WeaponData = FWeaponData();
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AWeaponBase::OnConstruction(const FTransform& Transform)
{
	
}

void AWeaponBase::SetWeaponData(FWeaponData *InData)
{
	SetupItemBasedOnDA(InData->WeaponDataAsset);
	WeaponData = InData;
	WeaponData->WeaponState = EWeaponState::WS_Unoccupied;
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
}

void AWeaponBase::ResetTimerBetweenShoots()
{
	WeaponData->WeaponState = EWeaponState::WS_Unoccupied;
	if(GetCurrentAmmo() <= 0)
	{
		Reload();
		GetWorldTimerManager().SetTimer(Timer_BlendFromIdleToShoot, this, &AWeaponBase::ShootingLoop, 0.1f);
	}
	else if(bAttackButtonPressed)
	{
		ShootingLoop();
	}
}

void AWeaponBase::PlayAttackSound()
{
	UGameplayStatics::PlaySoundAtLocation(this, GetData()->WeaponDataAsset->AttackSound, GetActorLocation());
}

void AWeaponBase::PlayAttackAnimation()
{
	if(WeaponOwner && GetData()->WeaponDataAsset->AttackMontage)
	{
		WeaponOwner->PlayAnimMontage(GetData()->WeaponDataAsset->AttackMontage);
	}
}

void AWeaponBase::SpawnShootParticles(FVector InShootEndPoint)
{
	UParticleSystem* TempHitEndParticle = GetData()->WeaponDataAsset->HitEndParticle;
	UParticleSystem* TempBeamParticle = GetData()->WeaponDataAsset->ShootBeamParticle;
	if(TempHitEndParticle)
	{
		// TODO: Wcześniej ustalić lepszą rotację na podstawie ImpactPoint z tracea (jeśli będzie taka potrzeba);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TempHitEndParticle, FTransform(FRotator(0.f), InShootEndPoint));
	}
	if(TempBeamParticle)
	{
		UParticleSystemComponent* Beam = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TempBeamParticle, GetFireSocketLocation());
		if(Beam)
		{
			// This parameter is definied in particle asset
			Beam->SetVectorParameter(FName("Target"), InShootEndPoint);
		}
	}
	//TODO: Spawn particle on end of rifle using socket on weapon skeletal mesh and particle from weapon data

}

FVector AWeaponBase::GetShootTracedPoint(FVector InCrosshairLocation, FVector InCameraLocation)
{
	// TODO: Większość tych rzeczy powinna być użyta do odpowiedniego ustawienia ręki przed strzałem, a podczas strzału tylko tracowanie na wprost
	FHitResult HitResult;
	FHitResult HitResultCrosshair;
	const FVector CharacterLocation = WeaponOwner->GetActorLocation();
	const FVector StartTraceFull = GetFireSocketLocation();
	const FVector EndTraceFull = WeaponOwner->GetActorForwardVector() * GetData()->WeaponDataAsset->Range + StartTraceFull;
	const FVector StartTraceCrosshair = InCrosshairLocation;
	const FVector EndTraceCrosshair = StartTraceCrosshair - (InCameraLocation-StartTraceCrosshair);

	bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, StartTraceFull, EndTraceFull, ECC_Visibility);
	bool bHitCrosshair = GetWorld()->LineTraceSingleByChannel(HitResultCrosshair, StartTraceCrosshair, EndTraceCrosshair, ECC_Visibility);
	
	const float DistToCrosshair = UKismetMathLibrary::Sqrt(FMath::Square(CharacterLocation.X- InCrosshairLocation.X)+FMath::Square(CharacterLocation.Y- InCrosshairLocation.Y));
	const float DistToHitPoint = UKismetMathLibrary::Sqrt(FMath::Square(GetActorLocation().X- HitResult.ImpactPoint.X)+FMath::Square(GetActorLocation().Y- HitResult.ImpactPoint.Y));

	FVector ShootRealEndLocation = EndTraceFull;
	if(bHitCrosshair && DistToCrosshair < DistToHitPoint && DistToCrosshair < 0.7f * GetData()->WeaponDataAsset->Range)
	{
		ShootRealEndLocation = HitResultCrosshair.ImpactPoint;
	}
	//DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor::Orange, false, 1.f);
	else if(bHit)
	{
		ShootRealEndLocation = HitResult.ImpactPoint;
	}
	else
	{
		// If no object on missle path, trace down to floor and
		// Shoot in this place - better looking when particles
		// spawn on floor than in air
		// Maybe in future I will have better idea;
		const FVector EndTraceFloor = EndTraceFull - FVector(0.f,0.f, 200.f);
		bool bHitFloor = GetWorld()->LineTraceSingleByChannel(HitResult, EndTraceFull, EndTraceFloor, ECC_Visibility);
		if(bHitFloor)
		{
			ShootRealEndLocation = HitResult.ImpactPoint;
		}
	}
	return ShootRealEndLocation;
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWeaponBase::DecreaseAmmoCount()
{	
	if(WeaponData->CurrentAmmo <= 0)
	{
		WeaponData->CurrentAmmo = 0;
	}
	else
	{
		WeaponData->CurrentAmmo--;
	}
}

FVector AWeaponBase::GetFireSocketLocation() const
{
	FVector SocketLocation = FVector(0.f, 0.f, 0.f);
	if(WeaponOwner)
	{
		SocketLocation = WeaponOwner->GetMesh()->GetSocketLocation(FName("HandMissleSocket"));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Weapon doesn't have weapon owner!")));
	}
	if(ItemSkeletalMesh->SkeletalMesh)
	{
		SocketLocation = ItemSkeletalMesh->GetSocketLocation(FName("FireSocket"));
	}
	else if(ItemStaticMesh->GetStaticMesh())
	{
		SocketLocation = ItemStaticMesh->GetSocketLocation(FName("FireSocket"));
	}
	return SocketLocation;
}

void AWeaponBase::StartShooting()
{
	bAttackButtonPressed = true;
	
	if(WeaponData)
	{
		// TODO: Zmienić na notify i wtedy wywalić tę zmienną
		GetWorldTimerManager().SetTimer(Timer_BlendFromIdleToShoot, this, &AWeaponBase::ShootingLoop, WeaponData->WeaponDataAsset->BlendTimeToShootingPose);
	}
}

void AWeaponBase::StopShooting()
{
	bAttackButtonPressed = false;
	GetWorldTimerManager().ClearTimer(Timer_BlendFromIdleToShoot);
}

void AWeaponBase::ShootingLoop()
{
	if(GetData()->WeaponDataAsset == nullptr) return;
	if(GetData()->WeaponState == EWeaponState::WS_Unoccupied)
	{
		if(GetCurrentAmmo() > 0 && bAttackButtonPressed)
		{
			DecreaseAmmoCount();
			PlayAttackSound();
			PlayAttackAnimation();
		
			// TODO: Spróbować inaczej pobrać pozycję kamery żeby nie castować.
			ASimeuronCharacter* TempSimeuronCharacter = Cast<ASimeuronCharacter>(WeaponOwner);
			if(TempSimeuronCharacter)
			{
				const FVector ShootTracedPoint = GetShootTracedPoint(TempSimeuronCharacter->GetCrosshair()->GetActorLocation(), TempSimeuronCharacter->GetFollowCamera()->GetComponentLocation());
				SpawnShootParticles(ShootTracedPoint);
				TempSimeuronCharacter->GetCrosshair()->Impulse();
			}

			// Set timer to unlock next shoot
			WeaponData->WeaponState = EWeaponState::WS_Shooting;
			GetWorldTimerManager().SetTimer(Timer_PauseBetweenShoots, this, &AWeaponBase::ResetTimerBetweenShoots, 1/GetData()->WeaponDataAsset->AttackRate);
		}
		else
		{
			Reload();
			GetWorldTimerManager().SetTimer(Timer_BlendFromIdleToShoot, this, &AWeaponBase::ShootingLoop, 0.1f);

		}
	}
	else
	{
		GetWorldTimerManager().SetTimer(Timer_BlendFromIdleToShoot, this, &AWeaponBase::ShootingLoop, 0.1f);
	}
	
	
}

void AWeaponBase::Reload()
{
	// TODO: Zastapic przez interfejs ktory bedzie pobieral equipment component
	ASimeuronCharacter* TmpSimeuronCharacter = Cast<ASimeuronCharacter>(WeaponOwner);
	if (TmpSimeuronCharacter == nullptr) return;
	if(GetData()->WeaponState != EWeaponState::WS_Unoccupied) return;
	if(GetData()->CurrentAmmo >= GetData()->WeaponDataAsset->MagazineCapacity) return;
	
	int AmmoInInventorty = TmpSimeuronCharacter->GetEquipmentComponent()->GetAmmoForCurrentWeapon();
	if(AmmoInInventorty > 0)
	{
		WeaponData->WeaponState = EWeaponState::WS_Reloading;
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Reload: START")));
		UEquipmentDefaults_DataAsset* TmpDefaults = TmpSimeuronCharacter->GetEquipmentComponent()->EquipmentDefaults;
		if(WeaponOwner && TmpDefaults && TmpDefaults->ReloadMontage)
		{
			const EWeaponType TmpWeaponType = GetData()->WeaponDataAsset->WeaponType;
			FName MontageSectionName = "Reload_test";
			if (TmpDefaults->ReloadSectionNames.Contains(TmpWeaponType))
			{
				MontageSectionName = *TmpDefaults->ReloadSectionNames.Find(TmpWeaponType);
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Not set reload section for: %s in EquipmentDefaults Data Asset. Check EquipmentComponent in Character BP"), *UEnum::GetValueAsString(TmpWeaponType));
			}
			
			UAnimInstance* AnimInstance = WeaponOwner->GetMesh()->GetAnimInstance();
			AnimInstance->Montage_Play(TmpDefaults->ReloadMontage);
			AnimInstance->Montage_JumpToSection(MontageSectionName);
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Reload: PlayedMontage")));
		}
	}
	else
	{
		// TODO: Play sound if no ammo in inventory;
	}
}

void AWeaponBase::InitAfterSpawn(ACharacter* Character)
{
	WeaponOwner = Character;
}

void AWeaponBase::FinishReloading()
{
	// TODO: Zastapic przez interfejs ktory bedzie pobieral equipment component
	ASimeuronCharacter* TmpSimeuronCharacter = Cast<ASimeuronCharacter>(WeaponOwner);
	if(TmpSimeuronCharacter)
	{
		const int MaxPossibleToAdd = GetData()->WeaponDataAsset->MagazineCapacity - GetData()->CurrentAmmo;
		int AmmoInInventorty = TmpSimeuronCharacter->GetEquipmentComponent()->GetAmmoForCurrentWeapon();
		int AmmoToAdd = FMath::Min(MaxPossibleToAdd, AmmoInInventorty);
		//TODO: odjac ammo z inventory
		TmpSimeuronCharacter->GetEquipmentComponent()->DecreaseAmmoForCurrentWeapon(AmmoToAdd);
		WeaponData->CurrentAmmo += AmmoToAdd;
	}
	WeaponData->WeaponState = EWeaponState::WS_Unoccupied;
}



