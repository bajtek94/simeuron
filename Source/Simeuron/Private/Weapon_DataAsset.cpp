// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_DataAsset.h"

FWeaponData::FWeaponData()
{
	WeaponDataAsset = nullptr;
	CurrentAmmo = 0;
	WeaponState = EWeaponState::WS_Unoccupied;	
}
FWeaponData::FWeaponData(UWeapon_DataAsset* DataAsset)
{
	WeaponDataAsset = DataAsset;
	CurrentAmmo = DataAsset->MagazineCapacity;
	WeaponState = EWeaponState::WS_Unoccupied;	
}