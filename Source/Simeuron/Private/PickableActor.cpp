// Fill out your copyright notice in the Description page of Project Settings.


#include "PickableActor.h"


#include "EquipmentComponent.h"
#include "SimeuronCharacter.h"
#include "Weapon_DataAsset.h"

APickableActor::APickableActor()
{
	DisplaySkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemDisplaySkeletal"));
	DisplaySkeletalMesh -> SetupAttachment(RootComponent);
	DisplaySkeletalMesh -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void APickableActor::OnConstruction(const FTransform& Transform)
{
	if(!ItemToPickupData)
		return;
	
	if(ItemToPickupData->ItemSkeletalMesh)
	{
		DisplaySkeletalMesh->SetSkeletalMesh(ItemToPickupData->ItemSkeletalMesh);
		DisplaySkeletalMesh->SetRelativeScale3D(FVector(DisplayMeshScale));
		DisplaySkeletalMesh->SetRelativeLocation(DisplayMeshLocationOffset);
		InteractionMesh->SetStaticMesh(nullptr);
	}
	else if(ItemToPickupData->ItemMesh)
	{
		InteractionMesh->SetStaticMesh(ItemToPickupData->ItemMesh);
		InteractionMesh->SetRelativeScale3D(FVector(DisplayMeshScale));
		InteractionMesh->SetRelativeLocation(DisplayMeshLocationOffset);
		DisplaySkeletalMesh->SetSkeletalMesh(nullptr);
	}
}

void APickableActor::BeginPlay()
{
	Super::BeginPlay();
}

void APickableActor::Interact_Implementation(AActor* ExecActorRef)
{
	if(!ensureAlwaysMsgf(ItemToPickupData, TEXT("No defined item (DA) in pickable actor.")))
		return;
	
	Super::Interact_Implementation(ExecActorRef);
	
	UEquipmentComponent* Equipment = Cast<UEquipmentComponent>(ExecActorRef->GetComponentByClass(UEquipmentComponent::StaticClass()));
	Equipment->AddItemToInventory(ItemToPickupData);
	
	Destroy();
}

void APickableActor::ShowWidget_Implementation(AActor* ExecActorRef)
{
	Super::ShowWidget_Implementation(ExecActorRef);
	
	// Apply highlight effect defined in post process
	DisplaySkeletalMesh->SetRenderCustomDepth(true);

	UpdateState(EIS_ToTake);
	UEquipmentComponent* Equipment = Cast<UEquipmentComponent>(ExecActorRef->GetComponentByClass(UEquipmentComponent::StaticClass()));
	if(!Equipment || !ItemToPickupData)
		return;

	// Pickup is Weapon
	if(const UWeapon_DataAsset* Weapon_DataAsset = Cast<UWeapon_DataAsset>(ItemToPickupData))
	{
		for (FWeaponData Weapon : Equipment->GetAllWeaponsData())
		{
			if(Weapon.WeaponDataAsset->WeaponType == Weapon_DataAsset->WeaponType)
			{
				UpdateState(EIS_ToReplace);
				break;
			}
		}
	}	
}

void APickableActor::HideWidget_Implementation(AActor* ExecActorRef)
{
	Super::HideWidget_Implementation(ExecActorRef);

	// Disable highlight effect defined in post process
	DisplaySkeletalMesh->SetRenderCustomDepth(false);
}
