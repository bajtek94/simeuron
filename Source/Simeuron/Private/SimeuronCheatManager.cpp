// Fill out your copyright notice in the Description page of Project Settings.


#include "SimeuronCheatManager.h"

#include "SimeuronCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"


void USimeuronCheatManager::SetWalkSpeed(float NewValue) const
{
	ASimeuronCharacter* PlayerCharacter = Cast<ASimeuronCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if(PlayerCharacter)
	{
		UCharacterMovementComponent* PlayerMovementComponent = Cast<UCharacterMovementComponent>(PlayerCharacter->GetMovementComponent());
		if(PlayerMovementComponent)
		{
			PlayerMovementComponent->MaxWalkSpeed = NewValue;
		}
	}
}
