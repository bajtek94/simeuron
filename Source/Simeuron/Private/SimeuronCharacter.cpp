// Copyright Epic Games, Inc. All Rights Reserved.

#include "SimeuronCharacter.h"

#include "DrawDebugHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/GameSession.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"

ASimeuronCharacter::ASimeuronCharacter()
{
	
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Character moves in the direction of input...	
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = false; // Rotate the arm based on the controller
	CameraBoom->bInheritPitch = false;
	CameraBoom->bInheritYaw = false;
	CameraBoom->bInheritRoll = false;
	
	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	
	// Add other components
	EquipmentComponent = CreateDefaultSubobject<UEquipmentComponent>(TEXT("EquipmentComponent"));

	// Setup default values
	bShootingMode = false;

	// Setup Anim instance
	AnimInstance = GetMesh()->GetAnimInstance();
}

void ASimeuronCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Setup references
	PlayerController = Cast<APlayerController>(GetController());
	AnimInstance = GetMesh()->GetAnimInstance();

	// Bind interaction trigger actions
	GetCapsuleComponent()-> OnComponentBeginOverlap.AddDynamic(this, &ASimeuronCharacter::OnBeginOverlapInteraction);
	GetCapsuleComponent()-> OnComponentEndOverlap.AddDynamic(this, &ASimeuronCharacter::OnEndOverlapInteraction);

	// Spawn crosshair
	if(Crosshair)
	{
		CrosshairActor = GetWorld()->SpawnActor<ACrosshair>(Crosshair, GetActorLocation(), FRotator(0,0,0));
		CrosshairActor->CharacterRef = this;
	}
	else{ UE_LOG(LogTemp, Error, TEXT("Crosshair is not setup!"));}

	// Save run speed value to make possible switching between run and walk speed
	// TODO: Szybkość biegania powinna być pobierana ze statystyk postaci
	RunSpeed = GetCharacterMovement()->MaxWalkSpeed;

	// Save default FOV to make possible switch between default and zoomed values
	if(FollowCamera)
	{
		CameraDefaultFOV = FollowCamera->FieldOfView;
		CameraCurrentFOV = CameraDefaultFOV;
	}
}

void ASimeuronCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//TODO: Tylko ręka powinna się obkręcać, a postać dopiero, gdy kąt celowania  będzie większy
	// Interpolate character rotation to crosshair
	if(CrosshairActor)
	{
		const FVector Crosshair2DLocation = FVector(CrosshairActor->GetActorLocation().X, CrosshairActor->GetActorLocation().Y, 0);
		const FVector Character2DLocation = FVector(GetActorLocation().X, GetActorLocation().Y, 0);
		const FRotator TargetRotation = FRotationMatrix::MakeFromX( Crosshair2DLocation -  Character2DLocation).Rotator();
		SetActorRotation(FMath::RInterpTo(GetActorRotation(), TargetRotation, DeltaSeconds, RotateSpeed));
		//SetActorRotation(FMath::RInterpConstantTo(GetActorRotation(), TargetRotation, DeltaSeconds, RotateSpeed));
	}

	// Check overlapping interaction points and find closest
	if(bInteractionFightCheck)
	{
		SetClosestInteractionPoint();
	}

	// Change FOV depends on character speed (walk or run);
	if(bIsWalking)
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraZoomedFOV, DeltaSeconds, CameraZoomSpeed);
	}
	else
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraDefaultFOV, DeltaSeconds, CameraZoomSpeed);
	}
	FollowCamera->SetFieldOfView(CameraCurrentFOV);
}

void ASimeuronCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASimeuronCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASimeuronCharacter::MoveRight);

	PlayerInputComponent->BindAxis("CrosshairMoveX", this, &ASimeuronCharacter::CrosshairMoveX);
	PlayerInputComponent->BindAxis("CrosshairMoveY", this, &ASimeuronCharacter::CrosshairMoveY);

	PlayerInputComponent->BindAxis("CrosshairMoveXMouse", this, &ASimeuronCharacter::CrosshairMoveXMouse);
	PlayerInputComponent->BindAxis("CrosshairMoveYMouse", this, &ASimeuronCharacter::CrosshairMoveYMouse);

	PlayerInputComponent->BindAction("Interaction", IE_Pressed, this, &ASimeuronCharacter::Interact);
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &ASimeuronCharacter::ShootStart);
	PlayerInputComponent->BindAction("Shoot", IE_Released, this, &ASimeuronCharacter::ShootStop);
	PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &ASimeuronCharacter::ToggleWalk);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &ASimeuronCharacter::SwitchWeaponNext);	
	PlayerInputComponent->BindAction("PreviousWeapon", IE_Pressed, this, &ASimeuronCharacter::SwitchWeaponPrevious);	
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ASimeuronCharacter::Reload);	

}


void ASimeuronCharacter::CrosshairMoveX(float Value)
{
	CrosshairActor->Move(Value, 0.f, true);
}
void ASimeuronCharacter::CrosshairMoveY(float Value)
{
	CrosshairActor->Move(0.f, Value , true);
}
void ASimeuronCharacter::CrosshairMoveXMouse(float Value)
{
	CrosshairActor->Move(Value,0.f, false);
}
void ASimeuronCharacter::CrosshairMoveYMouse(float Value)
{
	CrosshairActor->Move(0.f, Value, false);
}

void ASimeuronCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
}

void ASimeuronCharacter::OnBeginOverlapInteraction(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	IInteractionInterface* TmpInteractActor = Cast<IInteractionInterface>(OtherActor);
	if(TmpInteractActor)
	{
		if(CurrentInteractionObject == nullptr)
		{
			CurrentInteractionObject = TmpInteractActor;
			CurrentInteractionObject -> Execute_ShowWidget(Cast<UObject>(CurrentInteractionObject), this);
		}
		
		TArray<AActor*> OverlappingActors;
		GetCapsuleComponent() -> GetOverlappingActors(OverlappingActors);
		if(OverlappingActors.Num() > 1)
		{
			bInteractionFightCheck = true;
		}
	}
}
void ASimeuronCharacter::OnEndOverlapInteraction(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	IInteractionInterface* TmpInteractActor = Cast<IInteractionInterface>(OtherActor);
	if(TmpInteractActor)
	{
		if(TmpInteractActor == CurrentInteractionObject)
		{
			TmpInteractActor -> Execute_HideWidget(Cast<UObject>(CurrentInteractionObject), this);
		}
		TArray<AActor*> OverlappingActors;
		GetCapsuleComponent() -> GetOverlappingActors(OverlappingActors);
		if(OverlappingActors.Num() <= 0)
		{
			bInteractionFightCheck = false;
			CurrentInteractionObject = nullptr;
		}
		else if(OverlappingActors.Num() == 1)
		{
			bInteractionFightCheck = false;
			CurrentInteractionObject = Cast<IInteractionInterface>(OverlappingActors[0]);
			if(CurrentInteractionObject)
			{
				CurrentInteractionObject -> Execute_ShowWidget(Cast<UObject>(CurrentInteractionObject), this);
			}
		}
		else
		{
			SetClosestInteractionPoint();
		}
	}	
}

void ASimeuronCharacter::MoveForward(float Value)
{
	if(Value != 0)
	{
		const FRotator YawRotation(0, FollowCamera->GetComponentRotation().Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}
void ASimeuronCharacter::MoveRight(float Value)
{
	if(Value != 0)
	{
		const FRotator YawRotation(0, FollowCamera->GetComponentRotation().Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void ASimeuronCharacter::Interact()
{
	if(CurrentInteractionObject)
	{
		CurrentInteractionObject->Execute_Interact(Cast<UObject>(CurrentInteractionObject), this);
	}
	SetClosestInteractionPoint();
}


// TODO: Wszystko co zwiazane ze strzelaniem spróbować przenieść do klasy broni?
void ASimeuronCharacter::ShootStart()
{
	GetEquipmentComponent()->GetCurrentWeapon()->StartShooting();
	
	GetWorldTimerManager().ClearTimer(Timer_StayInShootingPose);
	if(!bShootingMode)
	{
		bShootingMode = true;
	}
}
void ASimeuronCharacter::ShootStop()
{
	GetEquipmentComponent()->GetCurrentWeapon()->StopShooting();

	// TODO: Może takie rzeczy powinny być w maszynie stanów? Zmiana pozycji po czasie.
	GetWorldTimerManager().SetTimer(Timer_StayInShootingPose, this, &ASimeuronCharacter::TurnOffShootingMode, 0.1f, false);
}

void ASimeuronCharacter::ToggleWalk()
{
	if(GetCharacterMovement()->MaxWalkSpeed == WalkSpeed)
	{
		GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
		bIsWalking = false;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
		bIsWalking = true;
	}
}

void ASimeuronCharacter::SwitchWeaponNext()
{
	GetEquipmentComponent()->SwitchWeaponNext();
}

void ASimeuronCharacter::SwitchWeaponPrevious()
{
	GetEquipmentComponent()->SwitchWeaponPrevious();
}

void ASimeuronCharacter::Reload()
{
	GetEquipmentComponent()->GetCurrentWeapon()->Reload();
}


void ASimeuronCharacter::TurnOffShootingMode()
{
	bShootingMode = false;
}


void ASimeuronCharacter::SetClosestInteractionPoint()
{
	if(!CurrentInteractionObject)
	{
		UE_LOG(LogTemp, Error, TEXT("Null interaction point!"));
	}
	TArray<AActor*> OverlappingActors;
	GetCapsuleComponent() -> GetOverlappingActors(OverlappingActors);
	
	if(OverlappingActors.Num() > 0)
	{
		AActor* Closest = OverlappingActors[0];
		for(AActor* a : OverlappingActors)
		{
			if(GetDistanceTo(a) < GetDistanceTo(Closest))
			{
				Closest = a;
			}
		}
		IInteractionInterface* ClosestInterface = Cast<IInteractionInterface>(Closest);
		if(CurrentInteractionObject && CurrentInteractionObject !=  ClosestInterface)
		{
			CurrentInteractionObject -> Execute_HideWidget(Cast<UObject>(CurrentInteractionObject), this);
			CurrentInteractionObject = ClosestInterface;
			if(CurrentInteractionObject)
			{
				CurrentInteractionObject -> Execute_ShowWidget(Cast<UObject>(CurrentInteractionObject), this);
			}
		}
		if(!CurrentInteractionObject->Execute_IsVisibleWidget(Cast<UObject>(CurrentInteractionObject), this))
		{
			CurrentInteractionObject -> Execute_ShowWidget(Cast<UObject>(CurrentInteractionObject), this);
		}
		
	}
	else if(CurrentInteractionObject)
	{
		CurrentInteractionObject -> Execute_HideWidget(Cast<UObject>(CurrentInteractionObject), this);
	}
	
}
