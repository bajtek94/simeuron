// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemBase.h"

// Sets default values
AItemBase::AItemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	ItemStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemStatic"));
	ItemStaticMesh -> SetupAttachment(RootComponent);
	ItemStaticMesh -> SetCollisionEnabled(ECollisionEnabled::NoCollision);

	ItemSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemSkeletal"));
	ItemSkeletalMesh -> SetupAttachment(RootComponent);
	ItemSkeletalMesh -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called when the game starts or when spawned
void AItemBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AItemBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AItemBase::SetupItemBasedOnDA(UItemBase_DataAsset* InItemData)
{
	ItemData = InItemData;
	if(ItemData && ItemStaticMesh && ItemSkeletalMesh)
	{
		USkeletalMesh* TempSkel = ItemData->ItemSkeletalMesh;
		if(TempSkel)
		{
			ItemSkeletalMesh -> SetSkeletalMesh(TempSkel);
			ItemStaticMesh -> SetStaticMesh(nullptr);
		}
		else
		{
			UStaticMesh* TempStatic = ItemData -> ItemMesh;
			if(TempStatic)
			{
				ItemStaticMesh -> SetStaticMesh(TempStatic);
				ItemSkeletalMesh -> SetSkeletalMesh(nullptr);
			}
		}
	}
}

