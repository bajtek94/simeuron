// Fill out your copyright notice in the Description page of Project Settings.


#include "GenericInteractionActor.h"

#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AGenericInteractionActor::AGenericInteractionActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	
	InteractWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
	InteractWidget-> SetupAttachment(RootComponent);
	InteractWidget->SetWidgetSpace(EWidgetSpace::Screen);

	Trigger = CreateDefaultSubobject<USphereComponent>(TEXT("Trigger"));
	Trigger -> SetupAttachment(RootComponent);
	Trigger -> SetSphereRadius(150.f);

	InteractionMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("InteractionMesh"));
	InteractionMesh -> SetupAttachment(RootComponent);
}

void AGenericInteractionActor::BeginPlay()
{
	Super::BeginPlay();
	UpdateState(InteractionState);
	Execute_HideWidget(this, this);
}

void AGenericInteractionActor::UpdateState(EInteractionState State)
{
	InteractionState = State;
	if(!ensureMsgf(InteractionInfoTable, TEXT("No assigned DataTable with interaction texts to interaction actor.")))
		return;
	
	FString ContextString;
	for(auto& Name : InteractionInfoTable->GetRowNames())
	{
		FInteractionStateInfo* RowInfo = InteractionInfoTable->FindRow<FInteractionStateInfo>(Name, ContextString);
		if(RowInfo && RowInfo->InteractionState == InteractionState)
		{
			InteractionSound = RowInfo->InteractionSound;
			InteractionText = RowInfo->InteractionText;
		}
	}		
}

void AGenericInteractionActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGenericInteractionActor::Interact_Implementation(AActor* ExecActorRef)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Interacted")));
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), InteractionSound, GetActorLocation());	
}

void AGenericInteractionActor::ShowWidget_Implementation(AActor* ExecActorRef)
{
	InteractWidget->SetVisibility(true);
	
	// Apply highlight effect defined in post process
	InteractionMesh->SetRenderCustomDepth(true);
}

void AGenericInteractionActor::HideWidget_Implementation(AActor* ExecActorRef)
{
	InteractWidget->SetVisibility(false);

	// Disable highlight effect defined in post process
	InteractionMesh->SetRenderCustomDepth(false);
}

void AGenericInteractionActor::LockInteraction_Implementation(AActor* ExecActorRef)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Interaction locked")));
}

void AGenericInteractionActor::UnlockInteraction_Implementation(AActor* ExecActorRef)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Interaction unlocked")));
}

bool AGenericInteractionActor::IsVisibleWidget_Implementation(AActor* ExecActorRef)
{
	return InteractWidget->IsVisible();
}
