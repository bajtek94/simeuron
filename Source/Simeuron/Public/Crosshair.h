// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/WidgetComponent.h"
#include "GameFramework/Actor.h"
#include "Crosshair.generated.h"

UCLASS()
class SIMEURON_API ACrosshair : public AActor
{
	GENERATED_BODY()
	
public:	
	ACrosshair();
	virtual void Tick(float DeltaTime) override;

	//* Impulse effect by scaling up */
    void Impulse();

	//* Move crosshair based on player input */
    void Move(float X, float Y, bool bGamepad);

	UFUNCTION(BlueprintPure)
	float GetMaxDistance(){ return MaxDistance; }

	UPROPERTY(VisibleDefaultsOnly)
	UWidgetComponent* CrosshairWidget;

	UPROPERTY()
	class ASimeuronCharacter* CharacterRef;

protected:
	virtual void BeginPlay() override;
	
private:
	//* Automatic run by timer after impulse effect */
	void StopImpulse();
	
	//* Run in tick with calculated Alpha (0-1) */	
	void SetScale(float Alpha);
	
	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess = "true"))
	float AdditionalScaleFromImpulse = 1.f;

	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess = "true"))
	float ScaleInterpSpeed = 10.f;

	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess = "true"))
	float SpeedMultiplierOnWalk = 0.6f;

	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess = "true"))
	float MaxDistance = 800.f;

	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess = "true"))
	float CursorSpeed_Gamepad = 1000.f;

	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess = "true"))
	float CursorSpeed_Mouse = 10.f;

	FVector2D BeginDrawSize;
	float CurrentAdditionalScaleFromImpulse = 0.f;
	FVector2D LastMovedOffset = FVector2D(0.f, 0.f);
	bool bScalingByImpulse = false;

	// TIMERS
	FTimerHandle Timer_RevertScale;
	float TimeToRevertAfterImpulse = 0.05f;
};
