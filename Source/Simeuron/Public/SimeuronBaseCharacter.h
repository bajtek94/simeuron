// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "HealthComponent.h"
#include "GameFramework/Character.h"
#include "SimeuronBaseCharacter.generated.h"

UCLASS()
class SIMEURON_API ASimeuronBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASimeuronBaseCharacter();
	void SpeedInterpolation(float NewSpeed);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UHealthComponent* HealthComponent;
	
private:
	float TargetSpeed = 0.f;
	bool bShouldInterpolating = false;	
	
};
