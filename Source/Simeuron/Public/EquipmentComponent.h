// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "EquipmentDefaults_DataAsset.h"
#include "WeaponBase.h"
#include "Components/ActorComponent.h"
#include "EquipmentComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SIMEURON_API UEquipmentComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEquipmentComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UWeapon_DataAsset* DefaultWeapon;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UEquipmentDefaults_DataAsset* EquipmentDefaults;

	UFUNCTION(BlueprintCallable)
    AWeaponBase* GetCurrentWeapon() { return CurrentWeaponRef;}

	UFUNCTION(BlueprintPure)
    TArray<FWeaponData> GetAllWeaponsData() const { return Weapons;}	
	
	UFUNCTION()
	void AddItemToInventory(UItemBase_DataAsset* Item);

	UFUNCTION()
    void SwitchWeaponNext();

	UFUNCTION()
    void SwitchWeaponPrevious();

	UFUNCTION(BlueprintCallable, BlueprintPure)
    int GetAmmoForCurrentWeapon();
	
	UFUNCTION()
    void DecreaseAmmoForCurrentWeapon(int InAmmoCount);
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	TArray<FWeaponData> Weapons;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TMap<EWeaponType, int32> Ammo;
	UPROPERTY()
	AWeaponBase* CurrentWeaponRef;
	int CurrentWeaponID;

	void AddWeaponToInventory(FWeaponData* InWeaponData);
	void SwitchWeapon(int Index);
};
