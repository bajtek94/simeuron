// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ItemBase.h"
#include "Weapon_DataAsset.h"

#include "WeaponBase.generated.h"

UCLASS()
class SIMEURON_API AWeaponBase : public AItemBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();
	virtual void OnConstruction(const FTransform& Transform) override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void DecreaseAmmoCount();
	void SetWeaponData (FWeaponData *InData);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurrentAmmo() const {return WeaponData->CurrentAmmo;}
	
	FWeaponData* GetData() const {return WeaponData;}

	UFUNCTION(BlueprintCallable)
    FVector GetFireSocketLocation() const;

	UFUNCTION(BlueprintCallable)
	void FinishReloading();

	void StartShooting();
	void StopShooting();
	void ShootingLoop();
	void Reload();

	void InitAfterSpawn(ACharacter* Character);
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	FWeaponData* WeaponData;
	UPROPERTY()
	ACharacter* WeaponOwner;

	FTimerHandle Timer_BlendFromIdleToShoot;
	FTimerHandle Timer_PauseBetweenShoots;
	bool bAttackButtonPressed = false;

	void ResetTimerBetweenShoots();

	void PlayAttackSound();
	void PlayAttackAnimation();
	FVector GetShootTracedPoint(FVector InCrosshairLocation, FVector InCameraLocation);
	void SpawnShootParticles(FVector InShootEndPoint);


};
