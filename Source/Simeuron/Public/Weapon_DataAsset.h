// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ItemBase_DataAsset.h"
#include "Sound/SoundCue.h"

#include "Weapon_DataAsset.generated.h"


/**
 * 
 */

class UWeapon_DataAsset;
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	WT_None		UMETA(DisplayName="NONE"),
    WT_Knife	UMETA(DisplayName="Knife"),
    WT_Pistol	UMETA(DisplayName="Pistol"),
    WT_Rifle	UMETA(DisplayName="Rifle")
};

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	WS_Unoccupied	UMETA(DisplayName = "Unoccupied"),
	WS_Reloading	UMETA(DisplayName = "Reloading"),
	WS_Shooting		UMETA(DisplayName = "Shooting"),
};


UCLASS(BlueprintType)
class SIMEURON_API UWeapon_DataAsset : public UItemBase_DataAsset
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponType WeaponType = EWeaponType::WT_None;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Range = 1000.f;
	/** Count of attack per second*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AttackRate = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 MagazineCapacity = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 AmmoInPickup = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ReloadTime = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BlendTimeToShootingPose = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundCue* AttackSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAnimMontage* AttackMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystem* HitEndParticle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystem* ShootBeamParticle;
	// TO ADD:
	// Rarity (enum); -> na ten moment jeszcze nie wiem czy będzie coś takiego w grze
	// TODO: Add equip and uneqip anims;
};

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_BODY()

	FWeaponData();
	FWeaponData(UWeapon_DataAsset* DataAsset);
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWeapon_DataAsset* WeaponDataAsset;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 CurrentAmmo = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	EWeaponState WeaponState = EWeaponState::WS_Unoccupied;	
};