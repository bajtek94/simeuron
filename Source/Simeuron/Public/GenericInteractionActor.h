// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "InteractionInterface.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "Engine/DataTable.h"
#include "GameFramework/Actor.h"
#include "GenericInteractionActor.generated.h"

UENUM(BlueprintType)
enum EInteractionState
{
	EIS_None		UMETA(DisplayName ="None"),
	EIS_ToUse		UMETA(DisplayName ="ToUse"),
	EIS_ToTake		UMETA(DisplayName ="ToTake"),
	EIS_ToReplace	UMETA(DisplayName ="ToReplace"),
	EIS_ToTalk		UMETA(DisplayName ="ToTalk"),
	EIS_Locked		UMETA(DisplayName ="Locked")	
};

/// FTableRowBase is needed to can create DT based on this structure
USTRUCT(BlueprintType)
struct FInteractionStateInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EInteractionState> InteractionState = EIS_ToUse;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FText InteractionText;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USoundBase* InteractionSound;
};

UCLASS()
class SIMEURON_API AGenericInteractionActor : public AActor, public IInteractionInterface
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	void UpdateState(EInteractionState State);

public:
	AGenericInteractionActor();
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
	USceneComponent* Root;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UWidgetComponent* InteractWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USphereComponent* Trigger;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* InteractionMesh;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FText InteractionText;
	
	UPROPERTY(EditAnywhere)
	UDataTable* InteractionInfoTable;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EInteractionState> InteractionState = EIS_ToUse;
	

	// Interaction Interface functions
	virtual void Interact_Implementation(AActor* ExecActorRef) override;
    virtual void ShowWidget_Implementation(AActor* ExecActorRef) override;
	virtual void HideWidget_Implementation(AActor* ExecActorRef) override;
	virtual void LockInteraction_Implementation(AActor* ExecActorRef) override;
	virtual void UnlockInteraction_Implementation(AActor* ExecActorRef) override;
	virtual bool IsVisibleWidget_Implementation(AActor* ExecActorRef) override;

private:
	UPROPERTY()
	USoundBase* InteractionSound;
	

};
