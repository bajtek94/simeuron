// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CheatManager.h"
#include "SimeuronCheatManager.generated.h"

/**
 * 
 */
UCLASS()
class SIMEURON_API USimeuronCheatManager : public UCheatManager
{
	GENERATED_BODY()
	
	public:
	UFUNCTION(Exec, meta = (OverrideNativeName = "Simeuron.Player.WalksSpeed"))
	void SetWalkSpeed(float NewValue) const;
	
};
