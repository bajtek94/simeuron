// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Weapon_DataAsset.h"
#include "Engine/DataAsset.h"
#include "EquipmentDefaults_DataAsset.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SIMEURON_API UEquipmentDefaults_DataAsset : public UDataAsset
{
	GENERATED_BODY()

	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAnimMontage* ReloadMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<EWeaponType, FName> ReloadSectionNames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<EWeaponType, USoundCue*> ReloadSounds;
	
};
