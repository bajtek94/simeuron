// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GenericInteractionActor.h"
#include "ItemBase.h"

#include "PickableActor.generated.h"

/**
 * 
 */
UCLASS()
class SIMEURON_API APickableActor : public AGenericInteractionActor
{	
	GENERATED_BODY()
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	APickableActor();

	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void Interact_Implementation(AActor* ExecActorRef) override;
	virtual void ShowWidget_Implementation(AActor* ExecActorRef) override;
	virtual void HideWidget_Implementation(AActor* ExecActorRef) override;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UItemBase_DataAsset* ItemToPickupData;

	UPROPERTY(BlueprintReadWrite)
	USkeletalMeshComponent* DisplaySkeletalMesh;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float DisplayMeshScale =1.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FVector DisplayMeshLocationOffset =FVector(0.f, 0.f, 50.f);

private:
	UPROPERTY()
	AItemBase* SpawnedItem;
	
};
