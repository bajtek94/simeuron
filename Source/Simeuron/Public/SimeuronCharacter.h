// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"


#include "Crosshair.h"
#include "EquipmentComponent.h"
#include "InteractionInterface.h"
#include "GameFramework/Character.h"
#include "SimeuronBaseCharacter.h"

#include "SimeuronCharacter.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShoot);

UCLASS(config=Game)
class ASimeuronCharacter : public ASimeuronBaseCharacter
{
	GENERATED_BODY()
	
public:
	ASimeuronCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Moving Parameters")
	bool bShootingMode = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Moving Parameters")
	float RotateSpeed = 2.f;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float WalkSpeed = 100.f;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ACrosshair> Crosshair;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Anim Parameters")
    float TimeToRestPoseAfterShoot = 2.f;
	
	virtual void PossessedBy(AController* NewController) override;
	
	UFUNCTION()
    void OnBeginOverlapInteraction(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
    void OnEndOverlapInteraction(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
	UEquipmentComponent* GetEquipmentComponent() { return EquipmentComponent; }

	//UFUNCTION()
	//void PlayAnimMontage(UAnimMontage* InAnimMontage) const;
	
	/*UPROPERTY(BlueprintAssignable)
	FOnShoot OnCharacterShoot;
*/
protected:

	ACrosshair* CrosshairActor;
	
	APlayerController* PlayerController;

	UAnimInstance* AnimInstance;
	

	void CrosshairMoveX(float Value);
	void CrosshairMoveY(float Value);
	void CrosshairMoveXMouse(float Value);
	void CrosshairMoveYMouse(float Value);

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	void Interact();

	void ShootStart();
	void ShootStop();

	void ToggleWalk();

	void SwitchWeaponNext();
	void SwitchWeaponPrevious();

	void Reload();
	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	//void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	//void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface


private:

	// COMPONENTS
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	/** Contains data and function to manage items and equipment */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UEquipmentComponent* EquipmentComponent;

	
	FTimerHandle Timer_StayInShootingPose;
	//FTimerHandle TimerForRate;
	bool bReadyToStopShooting = false;
	bool bReadyToNextShoot = true;
	bool bIsHoldingShootButton = false;

	UPROPERTY(BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	bool bIsWalking = false;

	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess = "true"))
	float CameraZoomedFOV = 90.f;
	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess = "true"))
	float CameraZoomSpeed = 20.f;

	float CameraDefaultFOV;
	float CameraCurrentFOV;
	
	void TurnOffShootingMode();

	float RunSpeed;

	FVector ShootHitParticleLocation;
	
	//TArray<IInteractionInterface*> InteractionObjects;
	IInteractionInterface* CurrentInteractionObject = nullptr;
	bool bInteractionFightCheck = false;

	void SetClosestInteractionPoint();

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	FORCEINLINE float GetRunSpeed() const { return RunSpeed; }
	UFUNCTION(BlueprintPure)
	FORCEINLINE ACrosshair* GetCrosshair() const { return CrosshairActor; }
	FORCEINLINE bool IsWalkingModeOn() const { return bIsWalking; }
	
};

