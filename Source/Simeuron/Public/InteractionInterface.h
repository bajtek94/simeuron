// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractionInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractionInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SIMEURON_API IInteractionInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Interact(AActor* ExecActorRef);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void ShowWidget(AActor* ExecActorRef);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void HideWidget(AActor* ExecActorRef);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void LockInteraction(AActor* ExecActorRef);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void UnlockInteraction(AActor* ExecActorRef);
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    bool IsVisibleWidget(AActor* ExecActorRef);

	
};
