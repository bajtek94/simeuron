// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ItemBase_DataAsset.h"
#include "GameFramework/Actor.h"
#include "ItemBase.generated.h"

UCLASS()
class SIMEURON_API AItemBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SetupItemBasedOnDA(UItemBase_DataAsset* InItemData);

	UPROPERTY(EditDefaultsOnly)
	USceneComponent* Root;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* ItemStaticMesh;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	UItemBase_DataAsset* ItemData;
	
	// Has higher priority than static mesh so will use only skeletal if both are set
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USkeletalMeshComponent* ItemSkeletalMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

};
