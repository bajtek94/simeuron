// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimeuronGameMode.generated.h"

UCLASS(minimalapi)
class ASimeuronGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASimeuronGameMode();
};



