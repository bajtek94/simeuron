// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Simeuron : ModuleRules
{
	public Simeuron(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"HeadMountedDisplay", 
			"GameplayAbilities", 
			"GameplayTags", 
			"GameplayTasks",
			"UMG",
			"Slate",
			"SlateCore"
		});
	}
}
